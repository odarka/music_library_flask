import sqlite3
from flask import Flask, render_template, request, redirect
from re import match

app = Flask(__name__)
DATA_BASE_FILE_NAME = 'tracks.db'

def check_duration(duration):
    if not match(r'^[1-9]\d*\:[0-5][0-9]$', duration):
        return "enter duration like 00:00"
    else:
        return ''


@app.route('/')
def index():
    with sqlite3.connect(DATA_BASE_FILE_NAME) as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM tracks")

        tracks = cursor.fetchall()
    return render_template('index.html', tracks=tracks)


@app.route('/add_track/', methods=['GET', 'POST'])
def add_track():
    if request.method == 'POST':
        duration = request.form["duration"]
        if check_duration(duration) != '':
            track = (request.form["performer"], request.form["track"], '')
            return render_template(
                'add_track.html',
                track=track,
                duration_error=check_duration(duration),
                is_add_track_page=True
            )

        with sqlite3.connect(DATA_BASE_FILE_NAME) as connection:
            query = f'INSERT INTO tracks (performer, track, duration) ' \
                    f'VALUES ("{request.form["performer"]}",' \
                    f' "{request.form["track"]}", ' \
                    f'"{duration}")'

            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
        return redirect('/')

    return render_template('add_track.html', track='', duration_errror='', is_add_track_page=True)


@app.route('/delete_track/', methods=['GET', 'POST'])
def delete_track():
    if request.method == 'POST':
        with sqlite3.connect(DATA_BASE_FILE_NAME) as connection:
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM tracks WHERE id={request.form['id_performer']}")
    return redirect('/')


@app.route('/search_track/', methods=['GET', 'POST'])
def search_track():
    if request.method == 'POST':
        with sqlite3.connect(DATA_BASE_FILE_NAME) as connection:
            query = f"SELECT * FROM tracks WHERE "
            search_lst = request.form['search'].split(' ')
            for word in search_lst:
                if word == search_lst[-1]:
                    query += f'id LIKE "%{word}%" or performer LIKE "%{word}%" or track LIKE "%{word}%" ' \
                             f'or duration LIKE "%{word}%" '
                else:
                    query += f'id LIKE "%{word}%"or performer LIKE "%{word}%" or track LIKE "%{word}%"' \
                             f' or duration LIKE "%{word}%" or'
            cursor = connection.cursor()
            cursor.execute(query)

        tracks = cursor.fetchall()
        return render_template('search_track.html', tracks=tracks, is_search_page=True)

    return render_template('search_track.html', tracks='', is_search_page=True)


@app.route('/edit_track/<int:id_track>/', methods=['GET', 'POST'])
def edit_track(id_track):
    with sqlite3.connect(DATA_BASE_FILE_NAME) as connection:
        cursor = connection.cursor()
        cursor.execute(f"SELECT * FROM tracks WHERE id={id_track}")

        track = cursor.fetchall()[0]

    if request.method == 'POST':
        duration = request.form["duration"]
        if check_duration(duration) != '':
            track = (id_track, request.form["performer"], request.form["track"], '')
            return render_template('edit_track.html', track=track, duration_error=check_duration(duration))

        with sqlite3.connect(DATA_BASE_FILE_NAME) as connection:
            query = f'UPDATE tracks SET performer="{request.form["performer"]}",' \
                    f' track="{request.form["track"]}", ' \
                    f'duration="{request.form["duration"]}"' \
                    f'WHERE id={id_track}'
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect('/')

    return render_template('edit_track.html', track=track)


if __name__ == '__main__':
    app.run(debug=True)
